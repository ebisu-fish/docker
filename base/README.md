# Ebisu Cardano Base
The base container used to build other containers and use the cardano-cli. The container uses a 
multi-stage docker build to cut down on the size of the final container. The final container is a 
debian:bullseye-slim base containing the cardano-node and cardano-cli binaries and their dependencies.

# Ebisu Cardano
Cardano Docker containers for running a stake pool and performing tasks related to node deployment and operation.

The `docker-compose` file contains various services for completing tasks associated with deploying a stake pool. Below 
is a brief description of some of those tasks.

## Running a Local Node
Some steps require having a local node running to complete. Run `docker-compose up node` to get the node running 
and wait for the node to fully sync.

## Create Keys and Addresses
First step to deploying a node will be to generate the stake pool keys and addresses as outlined in the 
[Creating keys and addresses](https://docs.cardano.org/projects/cardano-node/en/latest/stake-pool-operations/keys_and_addresses.html) 
section of the cardano docs.

Run `docker-compose up addgen` to generate the necessary files. Once you have the address and keys, fund the `payment.addr`
with enough ADA to cover the keyDeposit and transaction fees.

## Register address on the blockchain
The next step will be to register the stake address on the blockchain as outlined in the 
[Register stake address on the blockchain](https://docs.cardano.org/projects/cardano-node/en/latest/stake-pool-operations/register_key.html) 
section of the cardano docs.

Before you can complete this step be sure you have funded the payment address created in the previous step, and you have a node
running and fully synced. Once those things have been done, be sure to set the proper environment variables for the `addreg`
service. The variables can either be added to the service's environment section in the `docker-compose` file or inside a .env file.
The required variables are:
```.env
TRANSACTION_HASH=
TRANSACTION_IX=
ADDRESS_LOVELACE=
```
The variable's values can be obtained by running the following command once you have a node running and synced:
```shell script
docker-compose run node cardano-cli shelley query utxo --address ${YOUR_PAYMENT_ADDRESS} --mainnet
```

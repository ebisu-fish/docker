#!/bin/bash
set -e

echo "Setup"
cd data || exit 1
cardano-cli --version

echo "Generate Payment Key Pair"
cardano-cli shelley address key-gen --verification-key-file payment.vkey --signing-key-file payment.skey

echo "Generate Stake Key Pair"
cardano-cli shelley stake-address key-gen --verification-key-file stake.vkey --signing-key-file stake.skey

echo "Generate Payment Address"
cardano-cli shelley address build \
  --payment-verification-key-file payment.vkey \
  --stake-verification-key-file stake.vkey \
  --out-file payment.addr \
  --mainnet

echo "Generate Stake Address"
cardano-cli shelley stake-address build \
  --stake-verification-key-file stake.vkey \
  --out-file stake.addr \
  --mainnet

#!/bin/bash
set -e

## Setup
cd data || exit 1
cardano-cli --version

echo "Generating Cold Keys and a Cold_counter"
cardano-cli shelley node key-gen \
  --cold-verification-key-file cold.vkey \
  --cold-signing-key-file cold.skey \
  --operational-certificate-issue-counter-file cold.counter \

echo "Generating VRF Key pair"
cardano-cli shelley node key-gen-VRF --verification-key-file vrf.vkey --signing-key-file vrf.skey \

echo "Generating KES Key pair"
cardano-cli shelley node key-gen-KES --verification-key-file kes.vkey --signing-key-file kes.skey

echo "Generating Operational Certificate"
((SLOTS_PER_PERIOD=$(cat ../config/mainnet-shelley-genesis.json | jq -r '.slotsPerKESPeriod')))
echo "$SLOTS_PER_PERIOD"
((SLOT_NO=$(cardano-cli shelley query tip --mainnet | jq -r '.slotNo')))
echo "$SLOT_NO"
(( KES_PERIOD=SLOT_NO/SLOTS_PER_PERIOD ))
echo "$KES_PERIOD"
cardano-cli shelley node issue-op-cert \
       --kes-verification-key-file kes.vkey \
       --cold-signing-key-file cold.skey \
       --operational-certificate-issue-counter cold.counter \
       --kes-period $KES_PERIOD \
       --out-file node.cert

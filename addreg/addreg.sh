#!/bin/bash
set -e

echo "Setup"
cd data || exit 1
cardano-cli --version

echo "Get Protocol Parameters"
cardano-cli shelley query protocol-parameters --mainnet --out-file protocol.json

echo "Create Registration Certificate"
cardano-cli shelley stake-address registration-certificate --stake-verification-key-file stake.vkey --out-file stake.cert

echo "Draft Transaction"
cardano-cli shelley transaction build-raw \
  --tx-in "${TRANSACTION_HASH}#${TRANSACTION_IX}" \
  --tx-out $(cat payment.addr)+0 \
  --ttl 0 \
  --fee 0 \
  --out-file tx.raw \
  --certificate-file stake.cert

echo "Calculate Transaction Fee"
TRANSACTION_FEE=$(cardano-cli shelley transaction calculate-min-fee \
  --tx-body-file tx.raw \
  --tx-in-count 1 \
  --tx-out-count 1 \
  --witness-count 1 \
  --byron-witness-count 0 \
  --mainnet \
  --protocol-params-file protocol.json | cut -d' ' -f 1)
echo "TRANSACTION_FEE: ${TRANSACTION_FEE}"

echo "Calculate Deposit"
KEY_DEPOSIT=$(cat protocol.json | jq -r '.keyDeposit')
echo "KEY_DEPOSIT: ${KEY_DEPOSIT}"

echo "Calculate Change"
(( TRANSACTION_CHANGE=ADDRESS_LOVELACE-TRANSACTION_FEE-KEY_DEPOSIT ))
echo "TRANSACTION_CHANGE: ${TRANSACTION_CHANGE}"

echo "Submit Certificate with Transaction"
echo "Calculate TTL"
CURRENT_SLOT=$(cardano-cli shelley query tip --mainnet | jq -r '.slotNo')
(( TRANSACTION_TTL=CURRENT_SLOT+200 ))

echo "Building Transaction"
cardano-cli shelley transaction build-raw \
  --tx-in "${TRANSACTION_HASH}#${TRANSACTION_IX}" \
  --tx-out $(cat payment.addr)+$TRANSACTION_CHANGE \
  --ttl $TRANSACTION_TTL \
  --fee $TRANSACTION_FEE \
  --out-file tx.raw \
  --certificate-file stake.cert

echo "Sign Transaction"
cardano-cli shelley transaction sign \
  --tx-body-file tx.raw \
  --signing-key-file payment.skey \
  --signing-key-file stake.skey \
  --mainnet \
  --out-file tx.signed

echo "Submit Transaction"
cardano-cli shelley transaction submit \
  --tx-file tx.signed \
  --mainnet
